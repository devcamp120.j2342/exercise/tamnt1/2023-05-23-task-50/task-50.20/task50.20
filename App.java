
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class App {
    public static void main(String[] args) throws Exception {

        task1("Devcamp");
        task1(("Devcamp Uses").split(""));
        task1(new int[] { 1, 2, 3 });
        task2(3);
        task3(new int[] { 3, 8, 7, 6, 5, -4, -3, 2, 1 });
        task4(3);
        task5(new int[] { 1, 2, 3 }, new int[] { 1, 2, 5, 6 });
        task6(new Object[] { Double.NaN, 0, 15, false, "html", "develop", 47, null });
        task7(3);
        task8();
        task9(4, 7);

    }

    public static void task1(Object object) {
        System.out.println(isArray(object));
    }

    public static boolean isArray(Object object) {
        return object != null && object.getClass().isArray();
    }

    public static void task2(int n) {
        int[] nArray = { 1, 2, 3, 4, 5, 6 };
        for (var i = 0; i < nArray.length; i++) {
            var ele = nArray[i];
            if (i == n) {
                System.out.println(ele);
                return;
            }

        }
        System.out.println("null");
    }

    public static void task3(int[] array) {
        Arrays.sort(array);
        System.out.println(Arrays.toString(array));
    }

    public static void task4(int index) {
        int[] nArray = { 1, 2, 3, 4, 5, 6 };
        for (var i = 0; i < nArray.length; i++) {
            var ele = nArray[i];
            if (ele == index) {
                System.out.println("the index is:" + i);
                return;
            }

        }
        System.out.println("The index is: -1");

    }

    public static void task5(int[] array1, int[] array2) {
        int length1 = array1.length;
        int length2 = array2.length;
        int[] result = new int[length1 + length2];
        System.arraycopy(array1, 0, result, 0, length1);
        System.arraycopy(array2, 0, result, length1, length2);
        System.out.println(result);

    }

    public static void task6(Object[] object) {
        ArrayList<Object> filteredList = new ArrayList<Object>();
        for (Object obj : object) {
            if (obj instanceof String
                    || obj instanceof Number && !(obj instanceof Double && Double.isNaN((Double) obj))) {
                filteredList.add(obj);
            }
        }

        System.out.println("task 6:" + filteredList);

    }

    public static void task7(int n) {
        int[] nArray = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        List<Integer> filterArray = new ArrayList<>();
        for (int el : nArray) {
            if (el != n) {
                filterArray.add(el);
            }

        }
        System.out.println(filterArray);
    }

    public static void task8() {
        int[] randomArray = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        int randomNumber = (int) Math.floor(Math.random() * randomArray.length);
        System.out.println(randomNumber);
    }

    public static void task9(int x, int y) {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        while (arrayList.size() <= x) {
            arrayList.add(y);
        }

        System.out.println(arrayList);
    }

    public static void task10(int x, int y) {
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        while (arrayList.size() < y) {
            arrayList.add(x++);
        }

        System.out.println(arrayList);
    }

}
